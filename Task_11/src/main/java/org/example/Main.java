package org.example;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("birinci reqemi daxil edin: ");
        int num1 = scanner.nextInt();

        System.out.println("ikinci reqemi daxil edin: ");
        int num2 = scanner.nextInt();

        System.out.println("uchuncu reqemi daxil edin: ");
        int num3 = scanner.nextInt();

        System.out.println("dorduncu reqemi daxil edin: ");
        int num4 = scanner.nextInt();

        System.out.println("beshinci reqemi daxil edin: ");
        int num5 = scanner.nextInt();

        System.out.println("altinci reqemi daxil edin: ");
        int num6 = scanner.nextInt();

        int neticeA = num1 + num2 * num3;
        int neticeB = (num1 + num2) % 9;
        int neticeC = num1 + num2 * num3 / num4;
        int neticeD = num1 + num2 / num3 * num4 - num5 % num6;

        System.out.println(neticeA);
        System.out.println(neticeB);
        System.out.println(neticeC);
        System.out.println(neticeD);
    }
}