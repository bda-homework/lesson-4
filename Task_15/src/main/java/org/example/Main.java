package org.example;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("dereceni daxil edin: ");
        int farenqeyt = scanner.nextInt();

        int tselsiy = (farenqeyt - 32) * 5 / 9;

        System.out.println(farenqeyt + " degree Fahrenheit is equal to " + tselsiy + " in Celsius");

    }

}