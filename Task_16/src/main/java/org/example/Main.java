package org.example;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("inc daxil edin: ");
        int inc = scanner.nextInt();

        double meter = inc * 0.0254;

        System.out.println(inc + " inch is " + meter + " meters");

    }

}