package org.example;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("reqem daxil edin: ");
        int num = scanner.nextInt();

        int sum = 0;

        if (num > 0 && num < 1000) {
            while (num > 0) {
                sum = sum + num % 10;
                num = num / 10;
            }
        } else {
            System.out.println("reqem lakin 0 - 1000 araliginda ola biler");
        }
        System.out.println(sum);
    }

}