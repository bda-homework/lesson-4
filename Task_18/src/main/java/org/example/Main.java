package org.example;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("deqiqe daxil edin: ");
        int min = scanner.nextInt();

        int years = min / 365 / 24 / 60;
        int days = (min - (years * 365 * 24 * 60)) / 1440;

        if (years > 0) {
            System.out.println(min + " minutes is approximately " + years + " years and " + days + " days");
        } else {
            System.out.println(min + " minutes is approximately " + days + " days");
        }

    }

}