package org.example;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("birinci reqemi daxil edin: ");
        int num1 = scanner.nextInt();

        System.out.println("ikinci reqemi daxil edin: ");
        int num2 = scanner.nextInt();

        int max;
        int min;

        int netice1 = num1 + num2;
        int netice2 = num1 - num2;
        int netice3 = num1 * num2;
        int netice4 = (num1 + num2) / 2;
        int netice5 = num1 / num2;

        if (num1 > num2) {
            max = num1;
            min = num2;
        } else {
            max = num2;
            min = num1;
        }

        System.out.println("Sum of two integers: " + netice1);
        System.out.println("Difference of two integers: " + netice2);
        System.out.println("Product of two integers: " + netice3);
        System.out.println("Average of two integers: " + netice4);
        System.out.println("Distance of two integers: " + netice5);
        System.out.println("Max integer: " + max);
        System.out.println("Min integer: " + min);
    }

}