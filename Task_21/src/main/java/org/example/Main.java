package org.example;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        int num = 123456;

        while (num > 0) {
            int digit = num % 10;
            System.out.print(digit + " ");
            num /= 10;
        }
    }

}