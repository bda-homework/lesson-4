package org.example;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        int num = 1;
        int netice;
        for (int i = 1; i <= 10; i++) {
            for (int j = 1; j <= 10; j++) {
                netice = i * j;
                System.out.println(i + " x " + j + " = " + netice);
            }
            System.out.println();
        }
    }

}