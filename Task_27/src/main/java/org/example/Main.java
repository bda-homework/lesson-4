package org.example;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("reqem daxil edin: ");
        int num = scanner.nextInt();
        int count = 1;

        while (num > 0) {
            if (num / 10 > 0) {
                count = count + 1;
            }
            num = num / 10;
        }
        System.out.println(count);
    }

}