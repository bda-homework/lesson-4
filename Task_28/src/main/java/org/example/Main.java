package org.example;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("reqem daxil edin: ");
        int num = scanner.nextInt();
        int factorial = 1;

        for (int i = num; i > 0; i--) {
            factorial = factorial * i;
        }
        System.out.println(factorial);
    }

}