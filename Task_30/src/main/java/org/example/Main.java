package org.example;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("reqem daxil edin: ");
        int num = scanner.nextInt();
        int newNum = 0;

        while (num != 0) {
            int remainder = num % 10;
            newNum = newNum * 10 + remainder;
            num = num / 10;
        }

        if (newNum == (scanner.nextInt())) {
            System.out.println("duzdur");
        } else {
            System.out.println("sehvdir");
        }

    }

}