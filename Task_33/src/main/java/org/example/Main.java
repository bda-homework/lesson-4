package org.example;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("reqem daxil edin: ");
        int num = scanner.nextInt();

        for (int i = 2; i < num; i++) {
            boolean sadedir = true;
            for (int j = 2; j <= i / 2; j++) {
                if (i % j == 0) {
                    sadedir = false;
                    break;
                }
            }

            if (sadedir) {
                System.out.print(i + " ");
            }
        }
    }

}