package org.example;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("3-reqemli eded daxil edin: ");
        int number = scanner.nextInt();

        String[] birler = {"", "bir", "iki", "uc", "dord", "besh", "alti", "yeddi", "sekkiz", "doqquz"};
        String[] onlar = {"", "on", "iyirmi", "otuz", "qirx", "elli", "altmish", "yetmish", "seksen", "doxsan"};
        String[] teens = {"ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"};

        int yuz = number / 100;         //5
        int on = (number / 10) % 10;    //4
        int bir = number % 10;          //5

        System.out.print(number + " = ");
        if (yuz != 0) {
            System.out.print(birler[yuz] + " yuz ");
        }

        if (on != 0) {
            System.out.print(onlar[on] + " ");
        }

        if (bir != 0) {
            System.out.print(birler[bir] + " ");
        }
    }

}