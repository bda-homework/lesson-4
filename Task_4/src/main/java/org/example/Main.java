package org.example;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Reqemi daxil edin: ");
        int  num = scanner.nextInt();
        int bolunen = 2;

        for (int i = 2; i < num; i++) {
            if (num % i == 0) {
                bolunen++;
            }
        }

        if (bolunen > 2) {
            System.out.println("Reqem murekkebdir");
            System.out.println("Reqemin " + bolunen + " boluneni var");
        } else {
            System.out.println("Reqem sadedir");
        }
    }
}