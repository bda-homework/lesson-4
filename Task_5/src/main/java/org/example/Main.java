package org.example;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        String str = "aziza";
        String reversedStr = "";

        for (int i = str.length() - 1; i >= 0; i--) {
            reversedStr = reversedStr + str.charAt(i);
        }

        if (str.equals(reversedStr)) {
            System.out.println("Soz tersi ile eynidir");
        } else {
            System.out.println("Soz tersi ile eyni deyil");
        }
    }

}